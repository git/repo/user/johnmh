# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Monero (XMR) Stratum protocol proxy"
HOMEPAGE="https://xmrig.com https://github.com/xmrig/xmrig-proxy"

if [[ ${PV} == *9999 ]] ; then
	EGIT_REPO_URI="https://github.com/${PN}/${PN}.git"
	inherit git-r3
else
	SRC_URI="https://github.com/xmrig/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm64"
fi

LICENSE="Apache-2.0 GPL-3+ MIT"
SLOT="0"
IUSE="+ssl"

DEPEND="
	dev-libs/libuv:=
	ssl? ( dev-libs/openssl:= )
"
RDEPEND="${DEPEND}"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DWITH_TLS=$(usex ssl)
	)

	cmake_src_configure
}

src_install() {
	default
	dobin "${BUILD_DIR}/xmrig-proxy"
}
